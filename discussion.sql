-- Lists down the databses inside the DBMS
SHOW DATABASES;

-- Create a Database
CREATE DATABASE music_db;

--Drop Database(delete)
DROP DATABASE music_db;


-- Recreate Database
CREATE DATABASE music_db;


-- Incase need to delete a table
DROP TABLE table_name;

-- Select a Database
USE music_db;


-- Create Tables (users)
CREATE TABLE users (
	-- columns --
	id INT NOT NULL AUTO_INCREMENT,
	username VARCHAR(50) NOT NULL, 
	-- CHAR fix length
	-- (50) means max char
	-- NOT NULL means required
	password VARCHAR(50) NOT NULL,
	full_name VARCHAR(50) NOT NULL,
	contact_number VARCHAR(15) NOT NULL,
	email VARCHAR(50),
	address VARCHAR(50),
	PRIMARY KEY (id)
	-- primary key means cannot repeat it's value/ unique
);

-- NOT NULL means a column should have a value


CREATE TABLE artists (
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(50) NOT NULL,
	PRIMARY KEY(id)
);


CREATE TABLE albums (
	id INT NOT NULL AUTO_INCREMENT,
	album_title VARCHAR(50) NOT NULL,
	date_released DATE NOT NULL,
	artist_id INT NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_albums_artist_id -- constraint name
				-- declared column name    -- the reference for the column value
		FOREIGN KEY (artist_id) REFERENCES artists(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);


CREATE TABLE songs(
	id INT NOT NULL AUTO_INCREMENT,
	cong_name VARCHAR(50) NOT NULL,
	length TIME NOT NULL,
	genre VARCHAR(50) NOT NULL,
	album_id INT NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_songs_album_id
		FOREIGN KEY (album_id) REFERENCES albums(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);


CREATE TABLE playlists(
	id INT NOT NULL AUTO_INCREMENT,
	user_id INT NOT NULL, -- foreign key
	datetime_created DATETIME NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_playlists_user_id
		FOREIGN KEY (user_id) REFERENCES users(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);


-- 2 foreign keys
CREATE TABLE playlists_songs (
	id INT NOT NULL AUTO_INCREMENT,
	playlist_id INT NOT NULL,
	song_id INT NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_playlists_songs_playlist_id
		FOREIGN KEY (playlist_id) REFERENCES playlists(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT,
		-- constraint name fk_<table name>_<foreign key>
	CONSTRAINT fk_playlists_songs_song_id
			-- song_id - is a table column of playlists_songs
			-- that will use id of songs table as reference
		FOREIGN KEY (song_id) REFERENCES songs(id)
		ON UPDATE CASCADE
		-- upon updating a connected table it will also reflect the changes to this table specifically the id used as reference
		ON DELETE RESTRICT
);